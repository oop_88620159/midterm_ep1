/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.midterm_ep1;

/**
 *
 * @author acer
 */
public class MainTest {
    public static void main(String[] args) {
        Cutgrass cutgrass = new Cutgrass("some",15,150);
        Plantgrass plantgrass = new Plantgrass ("Sek",25,100);
        Nougrass nougrass = new Nougrass ("Naruto",55,80);
        
        cutgrass.Free();
        System.out.println(cutgrass);
        System.out.println(cutgrass.getP());
        System.out.println(cutgrass.getS());
        cutgrass.setS(20.8);
        System.out.println(cutgrass);
        System.out.println("--------------Finis test cutgrass--------------");
        
        
        System.out.println(plantgrass);
        System.out.println(plantgrass.getP());
        System.out.println(plantgrass.getS());
        plantgrass.setP(200.5);
        System.out.println(plantgrass);
        System.out.println("--------------Finis test plantgrass--------------");
        
        System.out.println(nougrass);
        nougrass.setP(120.5);
        System.out.println(nougrass);
        System.out.println(nougrass.getS());
        nougrass.setS(28.5);
        System.out.println(nougrass);
        System.out.println("--------------Finis test nougrass--------------");
        
    }
}
